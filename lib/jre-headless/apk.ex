defmodule Buildah.Apk.OpenJDK.JreHeadless do

    alias Buildah.{Apk, Apk.Catatonit, Cmd}

    def on_container(container, options) do
        Apk.packages_no_cache(container, [
        # Common most
            "openjdk11-jre-headless" #, # Version!!! also may differ for stable and edge Alpine version...
            # "util-linux" # recommended for Ammonite, Debian dep: https://packages.debian.org/en/bullseye/openjdk-11-jre-headless # removed because it can be quickly added with apk add then needed.
        ], options)
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v java"])
        {_, 0} = Cmd.config(
            container,
            env: [
                "DB_TYPE=sqlite" #?
            ],
            into: IO.stream(:stdio, :line)
        )
    end

    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v java"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "DB_TYPE"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c", "command -v java"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
    end

end

